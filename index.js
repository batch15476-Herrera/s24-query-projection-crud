
//find users with letter y in their first or last name show only email and is admin

db.users.find({$or:[{firstName:{$regex:'y',$options:'$i'}},{lastName:{$regex:'y',$options:'$i'}}]},{"_id":0,"email":1,"isAdmin":1})

//find users with letter e in their first and is an admin / show only email and is admin properties

db.users.find({$and:[{firstName:{$regex:'e',$options:'$i'}},{isAdmin:true}]},{"_id":0,"email":1,"isAdmin":1})

//find products with letter x in its name and has a price greater than orequal to 50000

db.products.find({$and:[{name:{$regex:'x',$options:'$i'}},{price:{$gte:50000}}]})

//update all products with price less than 2000 to inactive

db.products.updateMany({price:{$lt:2000}}, {$set:{isActive:false}})

//delete all products with price greater than 20000
db.products.deleteMany({price:{$gt:20000}})